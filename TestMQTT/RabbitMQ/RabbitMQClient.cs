﻿using System;
using RabbitMQ.Client;

namespace TestMQTT.RabbitMQ
{
    public class RabbitMQClient
    {
        private static ConnectionFactory _factory;
        private static IConnection _connection;
        private static IModel _model;

        private const string ExchangeName = "Topic_Exchange";
        private const string CardPaymentQueueName = "BufferStock";
        private const string PurchaseOrderQueueName = "GlobalStock";
        private const string AllQueueName = "AllTopic_Queue";

        public RabbitMQClient()
        {
            CreateConnection();
        }

        private static void CreateConnection()
        {
            _factory = new ConnectionFactory
            {
                HostName = "192.168.178.21",
                UserName = "guest",
                Password = "7Sevenb10Zehnc!"
            };

            _connection = _factory.CreateConnection();
            _model = _connection.CreateModel();
            _model.ExchangeDeclare(ExchangeName, "topic");

            _model.QueueDeclare(CardPaymentQueueName, true, false, false, null);
            _model.QueueDeclare(PurchaseOrderQueueName, true, false, false, null);
            _model.QueueDeclare(AllQueueName, true, false, false, null);

            _model.QueueBind(CardPaymentQueueName, ExchangeName, "weather.weatherpayment");
            _model.QueueBind(PurchaseOrderQueueName, ExchangeName,
                "weather.weathereorder");

            _model.QueueBind(AllQueueName, ExchangeName, "weather.*");
        }

        public void Close()
        {
            _connection.Close();
        }

        public void SendMessageto(WeatherForecast weather)
        {
            SendMessage(weather.Serialize(), "weather.weatherpayment");
            Console.WriteLine(" Payment Sent {0}, £{1}", weather.TemperatureC,
                weather.TemperatureF);
        }

        public void SendMessageOrder(WeatherForecast weatherorder)
        {
            SendMessage(weatherorder.Serialize(), "weather.weatherorder");

            Console.WriteLine(" Weather Order Sent {0}, £{1}, {2}, {3}",
                weatherorder.Summary, weatherorder.Date,
                weatherorder.TemperatureC, weatherorder.TemperatureF);
        }

        public void SendMessage(byte[] message, string routingKey)
        {
            _model.BasicPublish(ExchangeName, routingKey, null, message);
        }





    }
}
