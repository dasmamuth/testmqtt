﻿using System;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace TestMQTT.RabbitMQ
{
    public class RabbitMQDirectClient
    {
        private IConnection _connection;
        private IModel _channel;
        private string _replyQueueName;
        private EventingBasicConsumer _consumer;

        public void CreateConnection()
        {
            var factory = new ConnectionFactory { HostName = "localhost", UserName = "guest", Password = "guest" };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();

            _replyQueueName = _channel.QueueDeclare("rpc_reply", true, false, false, null);

            _consumer = new EventingBasicConsumer(_channel);
            _channel.BasicConsume(_replyQueueName, true, _consumer);
        }

        public void Close()
        {
            _connection.Close();
        }

        public string MakePayment(WeatherForecast weather)
        {
            var corrId = Guid.NewGuid().ToString();
            var props = _channel.CreateBasicProperties();
            props.ReplyTo = _replyQueueName;
            props.CorrelationId = corrId;

            _channel.BasicPublish("", "rpc_queue", props, weather.Serialize());

            while (true)
            {
                //var ea = _consumer.Received += Consumer



                //if (ea.BasicProperties.CorrelationId != corrId) continue;

                //var authCode = Encoding.UTF8.GetString(ea.Body);
                //return authCode;
                return ("");
            }
        }
    }
}
