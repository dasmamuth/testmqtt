﻿namespace TestMQTT.RabbitMQ.SimpleBus
{
    public abstract class Event<T>
    {
        public EventMeta Metadata { get; }
        public T Payload { get; }
    }
}
