﻿namespace TestMQTT.RabbitMQ.SimpleBus
{
    public class EventBusConfig : IEventBusConfig
    {
        public string Exchange { get; set; }
    }
}
