﻿using RabbitMQ.Client;
using System;
using Newtonsoft.Json;
using System.Text;

namespace TestMQTT.RabbitMQ.SimpleBus
{
    public class EventBusPublisher : IEventBusPublisher
    {
        private readonly IConnectionFactory _connectionFactory;
        private readonly IEventBusConfig _eventBusConfig;
        private IConnection _connection;

        public EventBusPublisher(
            IConnectionFactory connectionFactory, 
            IEventBusConfig eventBusConfig)
        {
            _connectionFactory = connectionFactory ?? throw new ArgumentNullException(nameof(connectionFactory));
            _eventBusConfig = eventBusConfig ?? throw new ArgumentNullException(nameof(eventBusConfig));
        }

        public void Connect()
        {
            _connection = _connectionFactory.CreateConnection();
        }

        public void Publish<TEvent>(TEvent @event) where TEvent : Event<object>
        {
            using var channel = _connection.CreateModel();
            var serialized = JsonConvert.SerializeObject(@event.Payload);
            var body = Encoding.UTF8.GetBytes(serialized);
            channel.BasicPublish(_eventBusConfig.Exchange, @event.Metadata.RoutingKey, null, body);
        }
    }
}
