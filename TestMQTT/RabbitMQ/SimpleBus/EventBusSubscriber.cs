﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Concurrent;
using System.Text;
using System.Threading.Tasks;

namespace TestMQTT.RabbitMQ.SimpleBus
{
    public class EventBusSubscriber : IEventBusSubscriber
    {
        private readonly IConnectionFactory _connectionFactory;
        private readonly IEventBusConfig _eventBusConfig;
        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger<EventBusSubscriber> _logger;
        private readonly ConcurrentDictionary<string, Type> _subscriptions;
        private IConnection _connection;
        private IModel _consumerChannel;

        public EventBusSubscriber(
            IConnectionFactory connectionFactory,
            IEventBusConfig eventBusConfig,
            IServiceProvider serviceProvider, 
            ILogger<EventBusSubscriber> logger)
        {
            _connectionFactory = connectionFactory ?? throw new ArgumentNullException(nameof(connectionFactory));
            _eventBusConfig = eventBusConfig ?? throw new ArgumentNullException(nameof(eventBusConfig));
            _serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _subscriptions = new ConcurrentDictionary<string, Type>();
        }

        public void Connect()
        {
            _connection = _connectionFactory.CreateConnection();
        }

        public void Subscribe<TEventMeta, THandler>(TEventMeta eventMeta) where TEventMeta : EventMeta where THandler : EventHandler
        {
            _consumerChannel ??= _connection.CreateModel();

            if (_subscriptions.TryAdd(eventMeta.RoutingKey, typeof(THandler)))
            {
                throw new Exception($"Subscribe attempt of {eventMeta.RoutingKey} was failed.");
            }

            var consumer = new EventingBasicConsumer(_consumerChannel);
            consumer.Received += async (model, ea) => await ConsumerOnReceived(_consumerChannel, ea);
            _consumerChannel.QueueBind(eventMeta.QueueName, _eventBusConfig.Exchange, eventMeta.RoutingKey);

        }

        private async Task ConsumerOnReceived(IModel channel, BasicDeliverEventArgs ea)
        {
            try
            {
                using var scope = _serviceProvider.CreateScope();
                
                if (!_subscriptions.TryGetValue(ea.RoutingKey, out var handlerType))
                {
                    _logger.LogWarning($"Handler for {ea.RoutingKey} was not found.");
                    channel.BasicReject(ea.DeliveryTag, false);
                }

                var handler = (EventHandler)scope.ServiceProvider.GetRequiredService(handlerType);
                var payload = Encoding.UTF8.GetString(ea.Body.ToArray());
                await handler.Handle(payload);

                channel.BasicAck(ea.DeliveryTag, false);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                channel.BasicReject(ea.DeliveryTag, false);
            }
        }
    }
}
