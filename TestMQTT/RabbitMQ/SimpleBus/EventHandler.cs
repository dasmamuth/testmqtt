﻿using System.Threading.Tasks;

namespace TestMQTT.RabbitMQ.SimpleBus
{
    public abstract class EventHandler
    {
        public abstract Task Handle(object payload);
    }
}
