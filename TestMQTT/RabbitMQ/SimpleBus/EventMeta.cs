﻿namespace TestMQTT.RabbitMQ.SimpleBus
{
    public abstract class EventMeta
    {
        public string RoutingKey { get; }
        public string QueueName { get;  }
    }
}
