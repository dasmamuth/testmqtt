﻿namespace TestMQTT.RabbitMQ.SimpleBus
{
    public interface IConnect
    {
        void Connect();
    }
}
