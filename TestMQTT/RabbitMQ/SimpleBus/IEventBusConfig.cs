﻿namespace TestMQTT.RabbitMQ.SimpleBus
{
    public interface IEventBusConfig
    {
        public string Exchange { get; set; }
    }
}
