﻿namespace TestMQTT.RabbitMQ.SimpleBus
{
    public interface IEventBusPublisher : IConnect
    {
        void Publish<TEvent>(TEvent @event) where TEvent : Event<object>;
    }
}
