﻿namespace TestMQTT.RabbitMQ.SimpleBus
{
    public interface IEventBusSubscriber : IConnect
    {
        void Subscribe<TEventMeta, THandler>(TEventMeta eventMeta) where TEventMeta : EventMeta where THandler : EventHandler;
    }
}
